..
   Copyright 2020-2022 David Robillard <d@drobilla.net>
   SPDX-License-Identifier: ISC

####
Lilv
####

.. include:: summary.rst

.. toctree::

   overview
   api/lilv
